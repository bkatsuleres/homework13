import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


/**
 * Created by Ben Katsuleres on 4/30/2017
 *
 * This is the controller for the FXML document that contains the view. 
 */
public class WxController implements Initializable {

  @FXML
  private Button btngo;

  @FXML
  private TextField txtzipcode;

  @FXML
  private Label lbllocation;
  
  @FXML
  private Label lbltime;

  @FXML
  private Label lblweather;

  @FXML
  private Label lbltemp;
  
  @FXML
  private Label lblwind;
  
  @FXML
  private Label lblpressure;
  
  @FXML
  private Label lblvisibility;

  @FXML
  private ImageView iconwx;

  @FXML
  private void handleButtonAction(ActionEvent e) {
    // Create object to access the Model
    WxModel weather = new WxModel();

    // Has the go button been pressed?
    if (e.getSource() == btngo)
    {
      String zipcode = txtzipcode.getText();
      if (weather.getWx(zipcode))
      {
        lbllocation.setText(weather.getLocation());
        lbltime.setText(weather.getTime());
        lblweather.setText(weather.getWeather());
        lbltemp.setText(String.valueOf(weather.getTemp()));
        lblwind.setText(weather.getWind());
        lblpressure.setText(weather.getPressure());
        lblvisibility.setText(weather.getVisibility());
        iconwx.setImage(weather.getImage());
      }
      else
      {
        lbllocation.setText("Invalid Zipcode");
        lbltime.setText("");
        lblweather.setText("");
        lbltemp.setText("");
        lblwind.setText("");
        lblpressure.setText("");
        lblvisibility.setText("");
        iconwx.setImage(new Image("badzipcode.png"));
      }
    }
  }

  @Override
  public void initialize(URL url, ResourceBundle rb) {
    // TODO
  }    

}
